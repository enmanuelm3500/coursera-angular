import { Component, OnInit } from '@angular/core';
import {DestinoViajeModel} from "../core/models/destino-viaje.model";

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  lista: DestinoViajeModel [];

  constructor() {
    this.lista = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre: string, url: string){
    this.lista.push(new DestinoViajeModel(nombre,url));
    console.log(this.lista);
    return false;
  }

  elegido(destino: DestinoViajeModel){
    this.lista.forEach( destino => {
      destino.setSelected(false);
    });
    destino.setSelected(true);
  }

}
