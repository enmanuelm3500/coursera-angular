export class DestinoViajeModel {
  nombre: string;
  imagen: string;
  private selected: boolean;
  servicios: string [];


  constructor(nombre: string, imagen: string) {
    this.nombre = nombre;
    this.imagen = imagen;
    this.servicios = ['desayuno', 'piscina']
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(value: boolean){
    this.selected = value;
  }


}
